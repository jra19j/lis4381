# LIS4381 - Mobile Web Application Development

## Jose Rafael Asmar

### Assignment 4 Requirements:

*Three parts:*

1. Create portfolio website
2. Skillsets 10, 11, and 12
3. Set up basic client-side validation

#### Assignment Screenshots:

#### Screenshot of Portfolio website:

![APP_Runn](img/portfolio.png "Concert app screenshot 1")

#### Screenshot of succesful client-side validation:

![APP_Runn2](img/clientval.png "Concert app screenshot 2")

#### Screenshot of non succesful client-side validation:

![ERD_dig](img/badval.png "My ERD")

#### Screenshot of Skillset 10:

![SS_4](img/SS10.png "Skillset 10")

#### Screenshot of Skillset 11:

![SS_5](img/SS11.png "Skillset 11")

#### Screenshot of Skillset 12:

![SS_6](img/SS12.png "Skillset 12")

<a id="raw-url" href="http://localhost:8080/lis4381/index.php">Portfolio site link!</a>
#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)