# LIS4381 - Mobile Web Application Development

## Jose Rafael Asmar

### Assignment 1 Requirements:

*Three parts:*

1. Distributed version control with git and Bitbucket
2. Development installations
3. Chapter Questions (Chs.1, 2)

#### git commands with short descriptions:
    
    - git init: used to start a new repository
    - git status: used to list files pending commit
    - git add: used to add a file to the staging area
    - git commit: used to commit files added using git add
    - git push: used to push committed files to repository
    - git pull: used to fetch repo changes to current working directory
    - git stash: used to temporarily store staged files

#### Assignment Screenshots:

#### Screenshot of PHP installation:

![PHP_Inst](img/PHP_Inst.png "My PHP Installation")

#### Screenshot of Java installation:

![java_PNG](img/java.png "My Java installation")

#### Screenshot of Android Studio My First App:

![F_APP_PNG](img/firstapp.png "My first app")

#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)