import java.util.*;

public class Methods {

 static void printIntro() {
        System.out.println("Developer: Jose R. Asmar");
        System.out.println("");
        System.out.println("Print minimum and maximum integer values.");
        System.out.println("Program prompts user to enter desired number of pseudorandom-generated integers (min 1).");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
        System.out.println("");
        System.out.println("Integer.MIN_VALUE = -2147483648");
        System.out.println("Integer.MAX_VALUE = 2147483647");
        System.out.println("");
    }

    public static int getNumberOfInts(){
        Scanner scnr = new Scanner(System.in);
        int UserChoice = 0;
        System.out.println("Enter desired number of pseudorandom-generated integers (min 1):");

        do {
            try {
                UserChoice = scnr.nextInt();
                while (UserChoice <= 0){
                    System.out.println("Enter a value above 0:");
                    UserChoice = scnr.nextInt();
                }
            } catch (InputMismatchException e){
                System.out.println("Numbers only, please enter a number:");
                scnr.next();
            }
        } while (UserChoice <= 0);

        return UserChoice;
    }

    public static void genNumbers() {
        int UserChoice = getNumberOfInts();
        Random rndm = new Random();
        int[] values = new int[UserChoice];

        System.out.println("For loop:");
        for (int i = 0; i < UserChoice; i++){
            int randomValue = rndm.nextInt();
            System.out.println(randomValue);
            }
        System.out.println("");

        System.out.println("Enhanced for loop:");
        for (int i = 0; i < UserChoice; i++){
            values[i] = rndm.nextInt();
        }
        for (int number: values) {
            System.out.println(number);
          }

        System.out.println("");

        System.out.println("While loop:");
          int count = 0;
          while (count < UserChoice){
              System.out.println(rndm.nextInt());
              count++;
          }
        System.out.println("");

        System.out.println("Do...while loop:");
        int count1 = 0;
        do {
            System.out.println(rndm.nextInt());
              count1++;
        } while (count1 < UserChoice);
    }

}