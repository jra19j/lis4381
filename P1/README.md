# LIS4381 - Mobile Web Application Development

## Jose Rafael Asmar

### Project 1 Requirements:

*Three parts:*

1. Create a mobile Business card app using Android Studio
2. Skillsets 7, 8, and 9
3. Answer chapter questions

#### Assignment Screenshots:

#### Screenshot of Business Card app running:
<p float="left">
<img src="img/Business_1.png" />
<img src="img/Business_2.png" />
</p>

#### Screenshot of Skillset 7:

![SS_7](img/SS7.png "Skillset 7")

#### Screenshot of Skillset 8:

![SS_8](img/SS8.png "Skillset 8")

#### Screenshot of Skillset 9:

![SS_9](img/SS9.png "Skillset 9")

#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)