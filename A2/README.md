# LIS4381 - Mobile Web Application Development

## Jose Rafael Asmar

### Assignment 2 Requirements:

*Three parts:*

1. Create a mobile recipe app using Android Studio
2. Skillsets 1, 2, and 3
3. Chapter Questions (Chs.3, 4)

#### Assignment Screenshots:

#### Screenshot of Recipe app running:

![APP_Runn](img/Recipe_1.png "Recipe app screenshot 1")

#### Screenshot of Recipe app running part 2:

![APP_Runn2](img/Recipe_2.png "Recipe app screenshot 2")

#### Screenshot of Skillset 1:

![SS_1](img/SS1.png "Skillset 1")

#### Screenshot of Skillset 2:

![SS_2](img/SS2.png "Skillset 2")

#### Screenshot of Skillset 3:

![SS_3](img/SS3.png "Skillset 3")

#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)