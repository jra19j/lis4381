import java.util.Scanner;

class Main
{
    public static void main(String args[])
    {
        System.out.println("Developer: Jose R. Asmar");
        System.out.println("Program loops through array of strings.");
        System.out.println("Use the following values: dog, cat, bird, fish, insect.");
        System.out.println("Use the following loop structures: for, enhanced for, while, do while.");
        System.out.println("\nNote: Pretest loops: for, enhanced for, while. Posttest loop: do-while.");
        System.out.println();

        String animals[] = {"dog", "cat", "bird", "fish", "insect"};

        System.out.println("for loop");
        for (int i = 0; i < animals.length; i++)
        {
            System.out.println(animals[i]);
        }

        System.out.println("\nenhanced for loop:");
        for (String test : animals)
        {
            System.out.println(test);
        }

        System.out.println("\nwhile loop:");
        int i=0;
        while (i < animals.length)
        {
            System.out.println(animals[i]);
            i++;
        }

        i=0;
        System.out.println("\ndo-while loop:");
        do 
        {
            System.out.println(animals[i]);
            i++;
        }
        while (i < animals.length);
    }
}