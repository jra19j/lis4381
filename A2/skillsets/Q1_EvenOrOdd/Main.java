import java.util.Scanner;

class Main 
{
 public static void main(String args[])
 {
    int num;
    System.out.println("Developer: Jose R. Asmar");
    System.out.println("Program evaluates integers as even or odd.");
    System.out.println("Note: Program does not check for non-numeric characters.");
    System.out.println();
    
    System.out.println("Enter integer: ");
    Scanner input = new Scanner(System.in);
    num = input.nextInt();

    if (num % 2 == 0)
        System.out.println(num + " is even.");
    else
        System.out.println(num + " is odd.");
 }   
}