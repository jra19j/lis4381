import java.util.Scanner;

class Main 
{
 public static void main(String args[])
 {
     Methods.getRequirements();

     System.out.println();
     int num1=0, num2=0;

     System.out.println("Enter first integer: ");
     num1 = Methods.getNum();

     System.out.println("Enter second integer: ");
     num2 = Methods.getNum();

     Methods.evaluateNumber(num1, num2);
 }   
}