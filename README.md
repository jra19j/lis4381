# LIS4381 - Mobile Web Application Development

## Jose Rafael Asmar

### LIS4381 Requirements:

*Course work links:*

1. [A1 README.md](A1/README.md "My A1 README file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocation)
    - Provide git command descriptions

2. [A2 README.md](A2/README.md "My A2 README file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
    - Complete skillsets 1, 2, 3

3. [A3 README.md](A3/README.md "My A3 README file")
    - Create ERD based upon buisness rules
    - Create concert ticketing android app
    - Complete skillsets 4, 5, 6

4. [A4 README.md](A4/README.md "My A4 README file")
    - Create portfolio website 
    - Set up basic client-side validation
    - Complete skillsets 10, 11, 12

5. [A5 README.md](A5/README.md "My A5 README file")
    - Demonstrate server-side validation
    - Complete Skillsets 13, 14, 15
    - Answer chapter questions

6. [P1 README.md](P1/README.md "My P1 README file")
    - Create a mobile Business card app using Android Studio
    - Skillsets 7, 8, and 9
    - Answer chapter questions

7. [P2 README.md](P2/README.md "My P2 README file")
    - Demonstrate Server-side validation
    - Create RSS Feed page
    - Answer chapter questions