# LIS4381 - Mobile Web Application Development

## Jose Rafael Asmar

### Project 2 Requirements:

*Three parts:*

1. Demonstrate Server-side validation
2. Create RSS Feed page
3. Answer chapter questions

#### Assignment Screenshots:

#### Screenshot of Petstore data list:

![APP_Runn](img/P2Data.png "data list")

#### Screenshot of edit failed validation:

![SS_13](img/P2editval.png "edit validation")

#### Screenshot of successful edit:

![Succ_ess](img/P2success.png "edit success")

#### Screenshot of Delete prompt:

![ADD_data](img/P2Delete.png "delete prompt")

#### Screenshot of Petstore after entry deleted:

![APP_Runn2](img/P2After.png "after deletion")

#### Screenshot of RSS Feed:

![RSS_Feed](img/RSSFeed.png "RSS Feed")

<a id="raw-url" href="http://localhost:8080/lis4381/index.php">Portfolio site link!</a>
#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)