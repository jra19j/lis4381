import java.util.Scanner;
import java.util.Random;
public class Methods {
    public static void printIntro(){

        System.out.println("Print minimum and maximum integer values.");
        System.out.println("Program prompts user to enter desired number of pseudorandom-generated integers (min 1).");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
        System.out.println("");
        System.out.println("Integer.MIN_VALUE = -2147483648");
        System.out.println("Integer.MAX_VALUE = 2147483647");
        System.out.println("");
    }

    public static void genNumber(){
        Random rndm = new Random();
        Scanner scnr = new Scanner(System.in);

        System.out.println("Enter desired number of pseudorandom-generated integers (min 1):");
        int numberDesired = scnr.nextInt();
        while (numberDesired < 1){
            System.out.println("The entered number must be atleast 1.");
            numberDesired = scnr.nextInt();
        }
        int[] values = new int[numberDesired];

        System.out.println("For loop:");
        for (int i = 0; i < numberDesired; i++){
            int randomValue = rndm.nextInt();
            System.out.println(randomValue);
            }
        System.out.println("");

        System.out.println("Enhanced for loop:");
        for (int i = 0; i < numberDesired; i++){
            values[i] = rndm.nextInt();
        }
        for (int number: values) {
            System.out.println(number);
          }

        System.out.println("");

        System.out.println("While loop:");
          int count = 0;
          while (count < numberDesired){
              System.out.println(rndm.nextInt());
              count++;
          }
        System.out.println("");

        System.out.println("Do...while loop:");
        
        int count1 = 0;
        do {
            System.out.println(rndm.nextInt());
              count1++;
        } while (count1 < numberDesired);
    }
    }

