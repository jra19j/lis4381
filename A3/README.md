# LIS4381 - Mobile Web Application Development

## Jose Rafael Asmar

### Assignment 3 Requirements:

*Three parts:*

1. Create a mobile concert ticket app using Android Studio
2. Skillsets 4, 5, and 6
3. Create an ERD using MySQL Workbench

#### Assignment Screenshots:

#### Screenshot of Ticket app running:

![APP_Runn](img/Concert_1.png "Concert app screenshot 1")

#### Screenshot of Ticket app running part 2:

![APP_Runn2](img/Concert_2.png "Concert app screenshot 2")

#### Screenshot of completed ERD:

![ERD_dig](img/ERDDiagram.png "My ERD")

#### Screenshot of populated tables:

![ERD_tbl](img/ERDTables.png "Populated tables")

#### Screenshot of Skillset 4:

![SS_4](img/SS4.png "Skillset 4")

#### Screenshot of Skillset 5:

![SS_5](img/SS5.png "Skillset 5")

#### Screenshot of Skillset 6:

![SS_6](img/SS6.png "Skillset 6")

#### A3 .mwb and .sql:
[A3 .mwb File](https://bitbucket.org/jra19j/lis4381/src/master/A3/ERD/A3.mwb "A3 ERD Files")
[A3 .sql File](https://bitbucket.org/jra19j/lis4381/src/master/A3/ERD/A3.sql.zip?format=zip)
<a id="raw-url" href="https://bitbucket.org/jra19j/lis4381/src/master/A3/ERD/A3.sql.zip">Download FILE</a>
#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)