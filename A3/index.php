<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Jose R. Asmar">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Create a mobile concert ticket app using Android Studio, Skillsets 4, 5, and 6, Create an ERD using MySQL Workbench 
				</p>

				<h4>Screenshot of Ticket app running</h4>
				<img src="img/Concert_1.png" class="img-responsive center-block" alt="Mobile Concert App Part 1">

				<h4>Screenshot of Ticket app running part 2</h4>
				<img src="img/Concert_2.png" class="img-responsive center-block" alt="Mobile Concert App Part 2">

				<h4>Screenshot of completed ERD</h4>
				<img src="img/ERDDiagram.png" class="img-responsive center-block" alt="Screenshot of completed ERD">
				
				<h4>Screenshot of populated tables</h4>
				<img src="img/ERDTables.png" class="img-responsive center-block" alt="Screenshot of populated tables">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
