# LIS4381 - Mobile Web Application Development

## Jose Rafael Asmar

### Assignment 5 Requirements:

*Three parts:*

1. Demonstrate Server-side validation
2. Skillsets 13, 14, and 15
3. Answer chapter questions

#### Assignment Screenshots:

#### Screenshot of Petstore data list:

![APP_Runn](img/A5Data.png "data list")

#### Screenshot of Petstore data list with an added entry:

![ADD_data](img/A5DataAdded.png "data list added entry")

#### Screenshot of non-succesful server-side validation:

![APP_Runn2](img/A5ServerError.png "server side")

#### Screenshot of non-succesful server-side validation:

![ERD_dig](img/A5ClientError.png "client side")

#### Screenshot of Skillset 13:

![SS_13](img/SS13.png "Skillset 13")

#### Screenshots of Skillset 14:

![SS_141](img/SS14-1.png "Skillset 14-1")
![SS_142](img/SS14-2.png "Skillset 14-2")
![SS_143](img/SS14-3.png "Skillset 14-3")
![SS_144](img/SS14-4.png "Skillset 14-4")

#### Screenshots of Skillset 15:

![SS_151](img/SS15-1.png "Skillset 15-1")
![SS_152](img/SS15-2.png "Skillset 15-2")

<a id="raw-url" href="http://localhost:8080/lis4381/index.php">Portfolio site link!</a>
#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)